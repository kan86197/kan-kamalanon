var score = 0;

var GameLayer = cc.LayerColor.extend({
  init: function() {
    this._super( new cc.Color( 127, 127, 127, 255 ) );
    this.setPosition( new cc.Point( 0, 0 ) );
    this.states = GameLayer.STATES.FRONT;
      
      
      
    this.time = 5;
    this.schedule(this.increaseSpeed, 1);
      
    this.background = new BackGround();
    this.background.setPosition( new cc.Point( 400, 300 ) )
    this.addChild(this.background);
    
    score = 0;
    this.scoreLabel = cc.LabelTTF.create( score + "", 'Arial', 20);
    this.scoreLabel.setPosition(new cc.Point( 780, 580 ) );
    this.addChild( this.scoreLabel );
    this.schedule(this.increaseScore, 1);
    
    this.man = new Man();
    this.man.setPosition( new cc.Point( 200, 400 ) );
    this.addChild(this.man);
    
    this.fence = new Obstacle();
    this.fence.setPosition( new cc.Point( 500, 320 ) );
    this.addChild(this.fence);
      
    this.floor = new Floor();
    this.floor.setPosition( new cc.Point( 300, 100 ) );
    this.addChild(this.floor);
      
    this.music();
    
    this.addKeyboardHandlers();
    this.man.scheduleUpdate();
    this.floor.scheduleUpdate();
    this.fence.scheduleUpdate();
    this.scheduleUpdate();
    return true;
  },
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },            
        }, this);
    },
    onKeyDown: function( keyCode, event ) {
        if( keyCode == 82 ){
            this.state = GameLayer.STATES.FRONT;
            this.stopGame();
            this.man.setPosition( new cc.Point( 200, 400 ) );
            this.fence.setPosition( new cc.Point( 500, 320, 2 ) );
            this.floor.setPosition( new cc.Point( 300, 100, 1 ) );
            this.man.scheduleUpdate();
            this.fence.scheduleUpdate();
            this.floor.scheduleUpdate();
            this.scheduleUpdate();
        }
        if ( keyCode == cc.KEY.right  && GameLayer.STATES.FRONT) {
            this.startGame();
            this.state = GameLayer.STATES.STARTED;
        } else if ( this.state == GameLayer.STATES.STARTED && keyCode == cc.KEY.up ) {
            this.man.jump();
        } 
    },
    onKeyUp: function( keyCode, event ) {},
    update: function( dt ) {
        if ( this.state == GameLayer.STATES.STARTED ){
            this.man.hitFloor(this.floor);
            this.man.move(this.floor);
            this.hitDetect(this.man, this.fence);
            this.fall();
            this.checkHitSky();
        } else if( this.state == GameLayer.STATES.DEAD ){
            this.stopGame();
            cc.director.runScene(new EndScene());
            this.unscheduleUpdate();
            this.fence.unscheduleUpdate();
            this.floor.unscheduleUpdate();
            this.unscheduleUpdate(this.increaseScore);
        }
    },
    startGame: function() {
        this.man.start();
        this.floor.start11();
        this.fence.startFence();
    },
    hitDetect: function(player, fence){
        var fencePos = this.fence.getPosition();
        var playerPos = this.man.getPosition();
        if( ( playerPos.x + 20 >= fencePos.x - 20 && playerPos.x - 20 <= fencePos.x + 20) && playerPos.y - 30 <= fencePos.y + 20 ){
            this.state = GameLayer.STATES.DEAD;
        }
    },
    stopGame: function(){
        this.man.stop();
        this.fence.stopFence();
        this.floor.stopFloor();
    },
    fall: function(){
        var playerPos = this.man.getPosition();
        if( playerPos.y - 20 <= 0){
            this.state = GameLayer.STATES.DEAD;
        }
    },
    increaseSpeed: function(){
        this.time--;
        if(this.time == 0){
            this.time = 5;
            this.fence.increaseFenceVelo();
            this.floor.increaseFloorVelo();
        }
    },
    increaseScore: function(){
        if(this.state == GameLayer.STATES.STARTED){
            score++;
            this.scoreLabel.setString(parseInt(score));
        }
    },
    music: function(){
        cc.audioEngine.playMusic( 'res/music/IntoTheBattleField.mp3' , true);
    },
    checkHitSky: function(){
        var manPos = this.man.getPosition();
        if(manPos.y + 30 > 600){
            this.stopGame();
            this.state = GameLayer.STATES.DEAD;
        }
    }
});


var StartScene = cc.Scene.extend({
  onEnter: function() {
    this._super();
    var layer = new GameLayer();
    layer.init();
    this.addChild( layer );
  }
});
GameLayer.STATES = {
    FRONT: 1,
    STARTED: 2,
    DEAD: 3
};
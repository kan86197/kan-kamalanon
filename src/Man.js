var Man = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/man.png' );
        this.velocityX = Man.STARTING_VELOCITY;
        this.velocityY = 0;
        this.started = false;
        this.onFloor = false;
    },
    update: function( dt ) {
        if( this.started ){
            console.log;
            this.velocityX = Man.STARTING_VELOCITY;
            var pos = this.getPosition();
            this.isMaxSpeed();
        }   else{
            this.velocityX = 0;
        }
    },
    jump: function(){
        this.velocityY = Man.JUMPING_VELOCITY;
    },
    start: function(){
        this.started = true;
    },
    stop: function(){
        this.started = false;
    },  
    move: function(floor){
        var pos = this.getPosition();
        var floorPos = floor.getPosition();
        if(this.onFloor){
            this.velocityY = 0;
            this.setPosition( new cc.Point( pos.x + this.velocityX, floorPos.y + 232 ) );
            }   else if(this.velocityY == 0 && this.onFloor){
                this.setPosition( new cc.Point( pos.x + this.velocityX, pos.y + this.velocityY ))
            }   else{
                this.velocityY += Man.G;
                this.setPosition( new cc.Point( pos.x + this.velocityX, pos.y + this.velocityY ))
            }
    },
    hitFloor: function(floor){
        var floorPosition = floor.getPosition();
        var pos = this.getPosition();
        if(pos.y - 30 <= floorPosition.y + 200 && (pos.x > floorPosition.x - 450 && pos.x < floorPosition.x + 450)){
            this.onFloor = true;
        }   else{
            this.onFloor = false;
        }
    },
    isMaxSpeed: function(){
        if(this.x >= 500){
                this.velocityX = 0;
            }   else{
                this.velocityX = Man.STARTING_VELOCITY;
            }
    },
    stop: function(){
        this.started = false;
        this.velocityX = 0;
        this.velocityY = 0;
    }
});
    Man.STARTING_VELOCITY = 10;
    Man.JUMPING_VELOCITY = 15;
    Man.G = -1;
    
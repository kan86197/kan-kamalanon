var EndPage = cc.Sprite.extend({
    init: function(){
        this._super();
        this.initWithFile( 'res/images/cityBackground.png' );
        this.setPosition(new cc.Point( 400, 300 ) );
        this.addKeyboardHandlers();
        
        this.music();
        this.wordLabel = cc.LabelTTF.create( "Don't give up", 'Arial', 40 );
        this.wordLabel.setPosition( new cc.Point( 400, 500 ) );
        
        this.instructLabel = cc.LabelTTF.create( "Press spacebar to keep RUNNING!!!", 'Arial', 40 );
        this.instructLabel.setPosition( new cc.Point( 400, 400 ) );
        
        this.scoreLabel = cc.LabelTTF.create( score + "", 'Arial', 80);
        this.scoreLabel.setPosition( new cc.Point( 400, 300 ) );
        this.addChild(this.scoreLabel);
        this.addChild(this.wordLabel);
        this.addChild(this.instructLabel);
    },
    
    addKeyboardHandlers: function(){
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event){
                self.onKeyDown(keyCode, event);
            }
        }, this);
    },
        onKeyDown: function( keyCode, event ){
            if(keyCode == 32){
                cc.director.runScene(new StartScene());
            }
    },
    music: function(){
        cc.audioEngine.playMusic( 'res/music/SadSong.mp3' , true);
    }
});


var EndScene = cc.Scene.extend({
  onEnter: function() {
    this._super();
    var layer = new EndPage();
    layer.init();
    this.addChild( layer );
  }
});
var FirstPage = cc.Sprite.extend({
    init: function(){
        this._super();
        this.initWithFile( 'res/images/cityBackground.png' );
        this.setPosition(new cc.Point( 400, 300 ) );
        this.addKeyboardHandlers();
        
        this.nameLabel = cc.LabelTTF.create( "Run for your life!", 'Arial', 30 );
        this.nameLabel.setPosition( new cc.Point( 400, 400) );
        
        this.instructLabel = cc.LabelTTF.create( "Press spacebar to start RUNNING!!!", 'Arial', 45 );
        this.instructLabel.setPosition( new cc.Point( 400, 300 ) );
        
        this.addChild(this.nameLabel);
        this.addChild(this.instructLabel);
    },
    
    addKeyboardHandlers: function(){
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event){
                self.onKeyDown(keyCode, event);
            }
        }, this);
    },
        onKeyDown: function( keyCode, event ){
            if(keyCode == 32){
                cc.director.runScene(new StartScene());
            }
    }
});


var FirstScene = cc.Scene.extend({
  onEnter: function() {
    this._super();
    var layer = new FirstPage();
    layer.init();
    this.addChild( layer );
  }
});
var Obstacle = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile('res/images/fence.png');
        this.start = false;
        this.velocity = 5;
    },
    update: function(dt){
        if(this.start){
        var pos = this.getPosition();
            if(pos.x + 350 < 0){
                this.setPosition( new cc.Point( 800 + 450, pos.y ) );
            }   else {
                this.setPosition( new cc.Point( pos.x - this.velocity, pos.y ) );
            }
        }
    }, 
    startFence: function(){
        this.start = true;
    },
    stopFence: function(){
        this.start = false;
    },
    randomFence: function(floor){
        var floorPos = floor.getPosition();
        var pos = this.getPosition();
        this.setPosition(new cc.point( new cc.Point( floorPos.x - 450 + (Math.random() * 700 ), pos.y)));
    },
    increaseFenceVelo: function(){
        this.velocity += 2;
    }
});
var Floor = cc.Sprite.extend({
    ctor: function(){
        this._super();
        this.initWithFile('res/images/floor.png');
        this.start = false;
        this.velocity = 5;
   },
    update: function( dt ){
     var pos = this.getPosition();
        if(this.start){
            if(pos.x + 450 < 0){
                this.setPosition( new cc.Point( 800 + 450, pos.y ) );
            }   else {
                this.setPosition( new cc.Point( pos.x - this.velocity, pos.y ) );
            }
        }
    },
    start11: function(){
        this.start = true;
    },
    stopFloor: function(){
        this.start = false;
    },
    increaseFloorVelo: function(){
        this.velocity += 2;
    }
});


var res = {
    man_png : "res/images/man.png",
    background_image : "res/images/cityBackground.png",
    sadSong_mp3 : "res/music/SadSong.mp3",
    intoTheBattleField_mp3 : "res/music/IntoTheBattleField.mp3" 
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
